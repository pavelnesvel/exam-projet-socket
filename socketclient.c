#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#define PORT 63611

int main(int argc, char const* argv[])
{
    int status, valread, client_fd;
    struct sockaddr_in serv_addr;
    char buffer[1024] = { 0 };
    const char* ip_address = "127.0.0.1"; // adresse IP par défaut
    if (argc >= 2) {
        ip_address = argv[1]; // utiliser l'adresse IP fournie en paramètre
    }
    if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary
    // form
    if (inet_pton(AF_INET, ip_address, &serv_addr.sin_addr)
        <= 0) {
        printf(
            "\nInvalid address/ Address not supported \n");
        return -1;
    }

    if ((status
         = connect(client_fd, (struct sockaddr*)&serv_addr,
                   sizeof(serv_addr)))
        < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    while (1) {
        // lire la commande à partir de l'entrée standard
        printf("Entrez une commande: ");
        fgets(buffer, 1024, stdin);

        // envoyer la commande au serveur
        send(client_fd, buffer, strlen(buffer), 0);

        // recevoir la réponse du serveur
        valread = read(client_fd, buffer, 1024);
        printf("%s\n", buffer);
    }

    // fermer la socket connectée
    close(client_fd);

    return 0;
}