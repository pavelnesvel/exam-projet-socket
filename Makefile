all: client server
client:
	gcc socketclient.c -o client 
server:
	gcc socket.c -o server
run:
	./server &
	./client $(SERVER_IP)